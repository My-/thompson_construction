from _ctypes import ArgumentError

from src.node import Node

__project__ = 'Thompson Construction'
__author__ = 'Mindaugas Sharskus'
__date__ = '09-03-2019'
__revisited__ = '22-03-2019'
__git__ = 'https://gitlab.com/My-/thompson_constructio'


class Graph:
    """
    Class for creating Graphs (collections of nodes)
    """
    def __init__(self, node=None):
        """
        Creates an empty Graph if no parameter ar given.
        :param node: if given creates Graph with a single node in it (mono-graph)
        """
        if isinstance(node, Node):
            self.__in = node
        else:
            self.__in = Node(node)

        self.__out = self.__in

    def get_start(self):
        """
        Gets graphs start (in) node.
        :return: start node of the graph
        """
        return self.__in

    def get_sink(self):
        """
        Gets sink (out) node of the graph.
        :return: sink node
        """
        return self.__out

    def set_start(self, node):
        """ Sets start node to a given one """
        if isinstance(node, Node):
            self.__in = node
        else:
            raise ArgumentError('Argument should be instance of Node')

    def set_sink(self, node):
        """ Sets sink node to a given one """
        if isinstance(node, Node):
            self.__out = node
        else:
            raise ArgumentError('Argument should be instance of Node')

    @staticmethod
    def build_and(graph1, graph2):
        """
        Builds an AND graph.

        --->[(in) graph1 ]--->[ graph2 ]---(out)>

        """
        if isinstance(graph1, Graph) and isinstance(graph2, Graph):
            g = Graph()
            g.__in = graph1.__in
            graph1.__out.add_child(graph2.__in)
            graph2.__out.add_child(g.__out)
        else:
            raise ArgumentError('Arguments should be instances of Graph. Got graph1: {}, graph2: {}'
                                .format(type(graph1), type(graph2)))

        return g

    @staticmethod
    def build_or(graph1, graph2):
        """
        Builds an OR graph.

             +--->[ graph1 ]---+
             |                 V
        -->(in)               (out)--->
             |                 A
             +--->[ graph2 ]---+
        """
        if isinstance(graph1, Graph) and isinstance(graph2, Graph):
            g = Graph()
            g.__out = Node()
            g.__in.add_child(graph1.__in)
            g.__in.add_child(graph2.__in)
            g.__out.add_parent(graph1.__out)
            g.__out.add_parent(graph2.__out)
        else:
            raise ArgumentError('Arguments should be instances of Graph')

        return g

    @staticmethod
    def build_none_or_more(graph):
        """
        Builds * (none or many) graph on given graph.

                +---->[ graph ]---+
                |                 |
            -->(in)<--------------+
                |
                +--------->( out )-->

        """
        if isinstance(graph, Graph):
            g = Graph()
            g.__in.add_child(graph.__in)
            g.__in.add_parent(graph.__out)
            g.__out = Node()
            g.__in.add_child(g.__out)
        else:
            raise ArgumentError('Arguments should be instances of Graph')

        return g

    @staticmethod
    def build_one_or_more(graph):
        """
        Builds + "one or many" graph on a given graph.

                +---->[ graph ]---+
                |                 V
            -->(in)<-------------(e)-----(out)--->

        """
        if isinstance(graph, Graph):
            g = Graph()
            e = Node()
            g.__out = Node()
            g.__in.add_child(graph.__in)
            graph.__out.add_child(e)
            e.add_child(graph.__in)
            e.add_child(g.__out)
        else:
            raise ArgumentError('Arguments should be instances of Graph')

        return g

    @staticmethod
    def build_one_or_none(graph):
        """
        Builds ? "one or none" graph on a given graph.

                +---->[ graph ]---+
                |                 V
            -->(in)------------>(out)-->

        """
        if isinstance(graph, Graph):
            g = Graph()
            g.__out = Node()
            g.__in.add_child(g.__out)
            g.__in.add_child(graph.__in)
            graph.__out.add_child(g.__out)
        else:
            raise ArgumentError('Arguments should be instances of Graph')

        return g

    @staticmethod
    def draw_graph(start_node, visited, graph):
        """
        "Draw" graph representation (print relationships between nodes). Use it for debugging.
        :param start_node: node to start in a graph
        :param visited: set holding visited nodes (to avoid cycles)
        :param graph: list were relationships will be stored
        """
        # Boiler code to allow IDE "see"
        # start_node = Node()
        # visited = set()
        # graph = ''

        if visited.__contains__(start_node):
            return

        visited.add(start_node)
        # print(start_node, start_node.get_children())
        graph.append('{} -> {} '.format(start_node, start_node.get_children()))

        if not start_node.has_children():
            return

        for ch in start_node.iterate_children():
            Graph.draw_graph(ch, visited, graph)

    def __repr__(self):
        visited = set()
        graph = list()
        Graph.draw_graph(self.__in, visited, graph)

        return "Graph: {0}".format(graph)
