from src.regex import ANY_SYMBOL

__project__ = 'Thompson Construction'
__author__ = 'Mindaugas Sharskus'
__date__ = '09-03-2019'
__revisited__  = '22-03-2019'
__git__ = 'https://gitlab.com/My-/thompson_constructio'


class Node:
    """
    Class representing node (vertices) in a graph.
    """

    def __init__(self, value=None):
        """
        Creates new Node instance.
        :param value: node value
        """
        self.__value = value
        self.__parents = set()
        self.__children = set()

    def is_empty(self):
        """
        Checks if node is empty.
        :return: true if node is empty
        """
        return self.__value is None

    def can_enter(self, value):
        """
        Checks if given parameter could "enter" node.
        Eg. matches nodes value of node is empty or marked as ANY
        :param value: value we matching against
        :return: true if given value can "enter" node.
        """
        return self.__value is value \
               or self.__value is None \
               or self.__value is ANY_SYMBOL

    def add_child(self, child):
        """
        Adds node as children to an existing node.
        Note: Current node is added as parent node to a children node.
        :param child: node to be "adopted"
        """
        self.__children.add(child)
        child.__parents.add(self)

    def add_parent(self, parent):
        """
        Adds this node as a child to a given node.
        :param parent: current node become a child to a given node.
        """
        self.__parents.add(parent)
        parent.__children.add(self)

    def has_children(self):
        """
        Checks if node has any children.
        :return: true if node has any children.
        """
        return len(self.__children) > 0

    def iterate_children(self):
        """ Iterates over children nodes """
        for ch in self.__children:
            yield ch

    def __repr__(self):
        return "Node({0})".format(self.__value if self.__value else '-' + hex(id(self))[-3:] + '-')


