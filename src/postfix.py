from src.regex import \
    BINARY_OPERATORS, \
    OPEN_BRACKETS, \
    SPECIALS
from src.regex import UNARY_OPERATORS
from src.regex import CLOSING_BRACKETS
from src.regex import AND
from src.regex import ANY
from src.stack import Stack

__project__ = 'Thompson Construction'
__author__ = 'Mindaugas Sharskus'
__date__ = '05-03-2019'
__revisited__ = '22-03-2019'
__git__ = 'https://gitlab.com/My-/thompson_construction'


class Postfix:
    """
    Class having only functions.
    """
    @staticmethod
    def andify(string):
        """
        Adds `AND` symbols (andify) in a given string.
        :param string: given string to be "andifyed".
        :return: "andifyed" string
        """
        r = Stack()
        not_end_before = [')', '|']
        not_end_before.extend(UNARY_OPERATORS.keys())

        for c in string:
            if r.peek() == '|':
                r.push(c)
            elif r.peek() == '(':
                r.push(c)
            elif c in not_end_before or r.is_empty():
                r.push(c)
            else:
                r.push(AND)
                r.push(c)

        return r.__str__()

    @staticmethod
    def to_postfix(regex):
        """
        Converts given infix regular expression to postfix.
        :param regex: infix regular expression
        :return: postfix regular expression
        """
        infix = list(regex)
        stack = Stack()
        postfix = Stack()

        for c in Postfix.andify(infix):
            if c in UNARY_OPERATORS or c == ANY:
                postfix.push(c)
            elif c in OPEN_BRACKETS:
                stack.push(c)
            elif c in CLOSING_BRACKETS:
                bracket_lvl = SPECIALS[c]
                while bracket_lvl != SPECIALS[stack.peek()]:
                    postfix.push(stack.pop())
                stack.pop()
            elif c in BINARY_OPERATORS:
                if stack.peek() in BINARY_OPERATORS and BINARY_OPERATORS[stack.peek()] <= BINARY_OPERATORS[c]:
                    postfix.push(stack.pop())
                stack.push(c)
            else:
                postfix.push(c)

        while not stack.is_empty():
            postfix.push(stack.pop())

        return postfix
