
__project__ = 'Thompson Construction'
__author__ = 'Mindaugas Sharskus'
__date__ = '05-03-2019'
__git__ = 'https://gitlab.com/My-/thompson_constructio'
__revisited__ = '22-03-2019'

# This file contains constants to be used for regular expression app

ANY = '.'
AND = '&'

NONE_OR_MORE_SYMBOL = '*'
ONE_OR_MORE_SYMBOL = '+'
ONE_OR_NONE_SYMBOL = '?'
ANY_SYMBOL = '.'
AND_SYMBOL = AND
OR_SYMBOL = '|'


BINARY_OPERATORS = {
    AND_SYMBOL: 6,
    OR_SYMBOL: 7
}

UNARY_OPERATORS = {
    NONE_OR_MORE_SYMBOL: 5,
    ONE_OR_MORE_SYMBOL: 5,
    ONE_OR_NONE_SYMBOL: 5
}

CLOSING_BRACKETS = {
    # ']': 3,
    ')': 4,
    # '}': 5
}

OPEN_BRACKETS = {
    # '[': 3,
    '(': 4,
    # '{': 5
}

SPECIALS = {
    '==': 1, '::': 1, '..': 1,
    '\\': 2,
    '[': 3, ']': 3,
    '(': 4, ')': 4,
    '*': 5, '+': 5, '?': 5, '{': 5, '}': 5,
    '': 6, '&': 6,            # and
    '^': 7, '$': 7,
    '|': 8              # or
}
