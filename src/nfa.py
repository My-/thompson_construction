from _ctypes import ArgumentError

from src.regex import \
    AND_SYMBOL, \
    OR_SYMBOL, \
    SPECIALS, \
    ANY_SYMBOL, \
    NONE_OR_MORE_SYMBOL,\
    ONE_OR_MORE_SYMBOL,\
    ONE_OR_NONE_SYMBOL
from src.stack import Stack
from src.graph import Graph
from src.postfix import Postfix

__project__ = 'Thompson Construction'
__author__ = 'Mindaugas Sharskus'
__date__ = '16-03-2019'
__revisited__ = '22-03-2019'
__git__ = 'https://gitlab.com/My-/thompson_constructio'


class NFA(Graph):
    """
    Non Deterministic Finite automata's class
    """
    def __init__(self):
        """
        Constructor for creating Non Deterministic Finite automata's
        """
        super().__init__()

    def build(self, regex):
        """
        Build NFA from a given regex.
        :param regex: Regular expression to be used for building NFA graph
        :raise: `ArgumentError` if stack contains not `Graph` elements
        """
        stack = Stack()
        postfix = Postfix.to_postfix(regex).__str__()

        for c in postfix:
            if c in SPECIALS:
                if c is AND_SYMBOL:
                    g2 = stack.pop()
                    g1 = stack.pop()
                    g = NFA.build_and(g1, g2)
                    stack.push(g)
                elif c is OR_SYMBOL:
                    g2 = stack.pop()
                    g1 = stack.pop()
                    g = NFA.build_or(g1, g2)
                    stack.push(g)
                elif c is NONE_OR_MORE_SYMBOL:
                    g1 = stack.pop()
                    g = NFA.build_none_or_more(g1)
                    stack.push(g)
                elif c is ONE_OR_MORE_SYMBOL:
                    g1 = stack.pop()
                    g = NFA.build_one_or_more(g1)
                    stack.push(g)
                elif c is ONE_OR_NONE_SYMBOL:
                    g1 = stack.pop()
                    g = NFA.build_one_or_none(g1)
                    stack.push(g)
            else:
                if c is ANY_SYMBOL:
                    g = Graph(ANY_SYMBOL)
                    stack.push(g)
                else:
                    g = Graph(c)
                    stack.push(g)

        g = stack.pop()
        if stack.is_empty() and isinstance(g, Graph):
            self.set_start(g.get_start())
            self.set_sink(g.get_sink())
        else:
            raise ArgumentError('Arguments should be instances of Graph')

    @staticmethod
    def follower(state, visited):
        """
        Follows nodes children. If children is empty (epsilon)
        then recursively continues to follow it's children.
        If node is not empty adds it to nodes set (states)
        :param state: node to be checked
        :param visited: set of visited nodes
        :return: nodes "at the end of the road"
        """
        states = set()

        if state.is_empty() and state.has_children():
            if visited.__contains__(state):
                return states
            visited.add(state)
            for child in state.iterate_children():
                states |= NFA.follower(child, visited)
        else:
            states.add(state)

        return states

    def check(self, string):
        """
        Checks if given string can traverse graph fully, get to
        accept state (node).
        :param string: string ve checking.
        :return: true if given string matches graph (regex)
        """
        current_nodes = set()
        current_nodes |= NFA.follower(self.get_start(), set())
        next_nodes = set()

        for c in string:
            for node in current_nodes:
                if node.can_enter(c):
                    for child in node.iterate_children():
                        next_nodes |= NFA.follower(child, set())

            current_nodes.clear()
            current_nodes |= next_nodes
            next_nodes.clear()

        # print(self.get_sink(), current_nodes)
        return self.get_sink() in current_nodes
