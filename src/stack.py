#
# Stack implementation in Python
# by: Mindaugas Sharskus
# 5-03-2019
#
# References:
#   - https://stackoverflow.com/questions/19151/build-a-basic-python-iterator

__project__ = 'Thompson Construction'
__author__ = 'Mindaugas Sharskus'
__date__ = '05-30-2019'
__revisited__ = '22-03-2019'
__git__ = 'https://gitlab.com/My-/thompson_constructio'


class Stack:
    """ Custom stack """

    def __init__(self):
        """ Creates an empty stack """
        self.__stack = list()

    def get_values(self):
        return self.__stack

    def is_empty(self):
        """ Checks stack for emptiness """
        return len(self.__stack) == 0

    def pop(self):
        """ Pops element from the stack """
        if not self.is_empty():
            return self.__stack.pop()

    def push(self, element):
        """ Adds element to the stack """
        self.__stack.append(element)

    def peek(self):
        """ Peeks at the stack """
        if not self.is_empty():
            return self.__stack[-1]

    def clear(self):
        """ Removes all elements from the stack """
        self.__stack.clear()

    def move_to(self, to):
        """ Moves all elements from stack to a given one """
        if isinstance(to, Stack):
            for e in self: to.push(e)
        else:
            for e in self: to.append(e)

    def iterate(self):
        """ Iterates over stack elements """
        while self.__stack:
            yield self.pop()

    def __iter__(self):
        """ Iterator """
        return self

    def __next__(self):
        """ Gets next element from the stack """
        if self.is_empty():
            raise StopIteration
        else:
            return self.pop()

    def __repr__(self):
        """ to string """
        return ''.join(self.__stack)
