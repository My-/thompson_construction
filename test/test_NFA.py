from unittest import TestCase

from src.nfa import NFA

__project__ = 'Thompson Construction'
__author__ = 'Mindaugas Sharskus'
__date__ = '16-03-2019'
__revisited__ = '27-03-2019'
__git__ = 'https://gitlab.com/My-/thompson_constructio'


class TestNFA(TestCase):

    def test_check_and(self):
        test = [
            # regex, string, result, message
            ('ab', 'ab', True, 'ab - ab'),
            ('ab', 'abb', False, 'ab - abb'),
            ('ab', 'bb', False, 'ab - bb'),
            ('abcd', 'abcd', True, 'abcd - abcd'),
            ('ab', 'aabb', False, 'ab - aabb'),
            ('ab', '', False, 'ab - NONE'),
            ('a.c', 'abc', True, 'a.c - abc'),
        ]
        nfa = NFA()

        for t in test:
            nfa.build(t[0])
            self.assertEqual(t[2], nfa.check(t[1]), t[3])

    def test_check_or(self):
        test = [
            # regex, string, result, message
            ('a|b', 'ab', False, 'a|b - ab'),
            ('a|b', 'abb', False, 'a|b - abb'),
            ('a|b', 'bb', False, 'a|b - bb'),
            ('a|b', 'aa', False, 'a|b - aa'),
            ('a|b', 'a', True, 'a|b - a'),
            ('a|b', 'b', True, 'a|b - b'),
            ('a|b', '', False, 'a|b - NONE'),
            ('a|b', '', False, 'a|b - NONE'),
            ('a|b|c', 'b', True, 'a|b|c - b'),
            ('a|b|c', 'c', True, 'a|b|c - c'),
            ('a|b|c', 'a', True, 'a|b|c - a'),
            ('a|.|c', 'F', True, 'a|.|c - F'),
        ]
        nfa = NFA()

        for t in test:
            nfa.build(t[0])
            self.assertEqual(t[2], nfa.check(t[1]), t[3])

    def test_check_and_or(self):
        test = [
            # regex, string, result, message
            ('a|bc', 'bc', True, 'a|bc - bc'),
            ('ac|b', 'acb', False, 'ac|b - acb'),
            ('a|bc|b', 'bc', True, 'a|bc|b - bc'),
            ('a|bc|b', 'a', True, 'a|bc|b - a'),
            ('a|bc|bd', 'bd', True, 'a|bc|b - bd'),
            ('a|bc|.d', 'fd', True, 'a|bc|.b - fd'),
        ]
        nfa = NFA()

        for t in test:
            nfa.build(t[0])
            self.assertEqual(t[2], nfa.check(t[1]), t[3])

    def test_check_and_or_brackets(self):
        test = [
            # regex, string, result, message
            ('(a|b)c', 'bc', True, '(a|b)c - bc'),
            ('(a|b)c', 'ac', True, '(a|b)c - ac'),
            ('a(c|b)', 'acb', False, 'a(c|b) - acb'),
            ('a(c|b)', 'ac', True, 'a(c|b) - ac'),
            ('a(c|b)', 'ab', True, 'a(c|b) - ab'),
            ('(a|b)(c|b)', '', False, '(a|b)(c|b) - None'),
            ('(a|b)(c|b)', 'cb', False, '(a|b)(c|b) - cb'),
            ('(a|b)(c|b)', 'a', False, '(a|b)(c|b) - a'),
            ('(a|b)(c|b)', 'ac', True, '(a|b)(c|b) - ac'),
            ('(a|b)(c|b)', 'bc', True, '(a|b)(c|b) - bc'),
            ('(a|b)(c|b)', 'bb', True, '(a|b)(c|b) - bb'),
            ('(a|b).(c|b)', 'bb', False, '(a|b).(c|b) - bb'),
            ('(a|b).(c|b)', 'bb', False, '(a|b).(c|b) - bfb'),
        ]
        nfa = NFA()

        for t in test:
            nfa.build(t[0])
            self.assertEqual(t[2], nfa.check(t[1]), t[3])

    def test_check_none_or_many(self):
        test = [
            # regex, string, result, message
            ('a*', '', True, 'a* - None'),
            ('a*', 'b', False, 'a* - b'),
            ('a*', 'a', True, 'a* - a'),
            ('a*', 'aa', True, 'a* - aa'),
            ('a*', 'aaa', True, 'a* - aaa'),
            ('a*', 'aaba', False, 'a* - aaba'),
            ('(a|b)*', '', True, '(a|b)* - None'),
            ('(a|b)*', 'a', True, '(a|b)* - a'),
            ('(a|b)*', 'b', True, '(a|b)* - b'),
            ('(a|b)*', 'ab', True, '(a|b)* - ab'),
            ('(a|b)*', 'aba', True, '(a|b)* - aba'),
            ('(a|b)*', 'abac', False, '(a|b)* - abac'),
            ('(a|a)*', 'ab', False, '(a|a)* - ab'),
            ('(a|b)*', 'aaabbb', True, '(a|b)* - aaabb'),
            ('(a|b)*', 'aaa', True, '(a|b)* - aaa'),
            ('(a|b)*', 'bbb', True, '(a|b)* - bbb'),
            ('(a|.)*', 'bbb', True, '(a|b)* - bbb'),
            ('(a|.)*', 'abcd', True, '(a|b)* - abcd'),
        ]
        nfa = NFA()

        for t in test:
            nfa.build(t[0])
            # print()
            # print(nfa)
            self.assertEqual(t[2], nfa.check(t[1]), t[3])

    def test_check_one_or_many(self):
        test = [
            # regex, string, result, message
            ('a+', '', False, 'a+ - None'),
            ('a+', 'b', False, 'a+ - b'),
            ('a+', 'a', True, 'a+ - a'),
            ('a+', 'aa', True, 'a+ - aa'),
            ('a+', 'aaa', True, 'a+ - aaa'),
            ('a+', 'aaba', False, 'a+ - aaba'),
            ('(a|b)+', '', False, '(a|b)+ - None'),
            ('(a|b)+', 'a', True, '(a|b)+ - a'),
            ('(a|b)+', 'b', True, '(a|b)+ - b'),

            ('(a|b)+', 'ab', True, '(a|b)+ - ab'),
            ('(a|b)+', 'aba', True, '(a|b)+ - aba'),
            ('(a|b)+', 'abac', False, '(a|b)+ - abac'),
            ('(a|a)+', 'ab', False, '(a|a)+ - ab'),
            ('(a|b)+', 'aaabbb', True, '(a|b)+ - aaabb'),
            ('(a|b)+', 'aaa', True, '(a|b)+ - aaa'),
            ('(a|b)+', 'bbb', True, '(a|b)+ - bbb'),
            ('(a|.)+', 'bbb', True, '(a|.)+ - bbb'),
            ('(a|.)+', 'abcd', True, '(a|.)+ - abcd'),
        ]
        nfa = NFA()

        for t in test:
            nfa.build(t[0])
            # print()
            # print(nfa)
            self.assertEqual(t[2], nfa.check(t[1]), t[3])

    def test_check_one_or_none(self):
        test = [
            # regex, string, result, message
            ('a?', '', True, 'a? - None'),
            ('a?', 'b', False, 'a? - b'),
            ('a?', 'a', True, 'a? - a'),
            ('a?', 'aa', False, 'a? - aa'),
            ('a?', 'aaa', False, 'a? - aaa'),
            ('a?', 'aaba', False, 'a? - aaba'),
            ('(a|b)?', '', True, '(a|b)? - None'),
            ('(a|b)?', 'a', True, '(a|b)? - a'),
            ('(a|b)?', 'b', True, '(a|b)? - b'),

            ('(a|b)?', 'ab', False, '(a|b)? - ab'),
            ('(a|b)?', 'aba', False, '(a|b)? - aba'),
            ('(a|b)?', 'abac', False, '(a|b)? - abac'),
            ('(a|a)?', 'ab', False, '(a|a)? - ab'),
            ('(a|b)?', 'aaabbb', False, '(a|b)? - aaabb'),
            ('(a|b)?', 'aaa', False, '(a|b)? - aaa'),
            ('(a|b)?', 'bbb', False, '(a|b)? - bbb'),
            ('(a|.)?', 'bbb', False, '(a|.)? - bbb'),
            ('(a|.)?', 'abcd', False, '(a|.)? - abcd'),

            ('(a|.)?', 'f', True, '(a|.)? - f'),
        ]
        nfa = NFA()

        for t in test:
            nfa.build(t[0])
            # print()
            # print(nfa)
            self.assertEqual(t[2], nfa.check(t[1]), t[3])

    def test_check_complex(self):
        test = [
            # regex, string, result, message
            ('(a|.)+', 'fakh', True, '(a|.)+ - fakh'),
            ('ab(c|d)', 'abc', True, 'ab(c|d) - abc'),
            ('ab(c|d)', 'abd', True, 'ab(c|d) - abd'),
            ('ab(c|d)', 'abcd', False, 'ab(c|d) - abcd'),
            ('ab(c|d)', 'acd', False, 'ab(c|d) - acd'),
            ('ab|(c|d)', 'ab', True, 'ab|(c|d) - ab'),
            ('ab|(c|d)', 'c', True, 'ab|(c|d) - c'),
            ('ab|(c|d)', 'd', True, 'ab|(c|d) - d'),
            ('(ab|(c|d))+', 'abcddcabab', True, 'ab|(c|d)+ - abcddcabab'),
            ('ab|(c|d)*', 'dabddaababc', False, 'ab|(c|d)* - dabddaababc'),
            ('a**', 'aaaaa', True, 'ab|(c|d)* - dabddaababc'),
            ('b**', 'aaaaa', False, 'ab|(c|d)* - dabddaababc'),
            ('a****', 'aaaaa', True, 'ab|(c|d)* - dabddaababc'),
            ('a**+**', 'a', True, 'ab|(c|d)* - dabddaababc'),
            ('(w/x)* ', 'wxx', False, 'ab|(c|d)* - dabddaababc'),
            ('(w|x)*', 'wxx', True, 'ab|(c|d)* - dabddaababc'),
        ]
        nfa = NFA()

        for t in test:
            nfa.build(t[0])
            # print()
            # print(nfa)
            self.assertEqual(t[2], nfa.check(t[1]), t[3])
