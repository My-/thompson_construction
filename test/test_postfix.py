from unittest import TestCase

from src.regex import AND
from src.postfix import Postfix


class TestPostfix(TestCase):
    def test_andify(self):
        test = [
            ('ab(a|c*)|(a+b*)bc*def', 'a&b&(a|c*)|(a+&b*)&b&c*&d&e&f'),
        ]

        for t in test:
            self.assertEqual(t[1], Postfix.andify(t[0]), t[0])

    def test_to_postfix(self):
        test = [
            'b((a?b*)|(e|aba))', 'ba?b*{0}eab{0}a{0}||{0}'.format(AND),
            'a|b|c|d', 'ab|c|d|',
            'a+b*c+d', 'a+b*{0}c+{0}d{0}'.format(AND),
            '(ab)|(c*d)', 'ab{0}c*d{0}|'.format(AND),
            '(a.)|(c*d).', 'a.{0}c*d{0}.{0}|'.format(AND),
            'b(a?b*)|(c+b*h..c).(d|h).', 'ba?b*{0}{0}c+b*{0}h{0}.{0}.{0}c{0}.{0}dh|{0}.{0}|'.format(AND),
        ]

        for i in range(0, len(test), 2):
            self.assertEqual(test[i + 1], Postfix.to_postfix(test[i]).__str__())
