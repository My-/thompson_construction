from unittest import TestCase

from src.graph import Graph


class TestGraph(TestCase):
    def test_draw_graph(self):
        a = Graph('a')
        b = Graph('b')
        c = Graph('c')
        d = Graph('d')

        g1 = Graph.build_and(a, b)
        g2 = Graph.build_or(c, d)

        g = Graph.build_or(g1, g2)
        graph = list()

        Graph.draw_graph(g.get_start(), set(), graph)

        print('\n'.join(graph))
        self.fail()

    def test_draw_graph_and(self):
        a = Graph('a')
        b = Graph('b')
        c = Graph('c')
        d = Graph('d')

        g = Graph.build_and(a, b)
        # g2 = Graph.build_or(c, d)

        # g = Graph.build_or(g1, g2)
        graph = list()

        Graph.draw_graph(g.get_start(), set(), graph)

        print('\n'.join(graph))
        self.fail()
