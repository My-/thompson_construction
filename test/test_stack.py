from unittest import TestCase

from src.stack import Stack

__project__ = 'Thompson Construction'
__author__ = 'Mindaugas Sharskus'
__date__ = '05-03-2019'
__revisited__ = '28-03-2019'
__git__ = 'https://gitlab.com/My-/thompson_constructio'


class TestStack(TestCase):
    """ Tests for custom Stack implementation """

    def test_isEmpty(self):
        stack = Stack()
        self.assertTrue(stack.is_empty(), 'Empty stack')

        stack.get_values().append('Hello')
        stack.get_values().append('My')
        stack.get_values().append('Stack')
        self.assertFalse(stack.is_empty(), 'Not empty stack')

    def test_pop(self):
        stack = Stack()
        test = ['Hello', 'My', 'Stack']
        for t in test:
            stack.get_values().append(t)

        self.assertEqual(test[2], stack.pop(), 'testing pop()')
        self.assertEqual(test[1], stack.pop(), 'testing pop()')
        self.assertEqual(test[0], stack.pop(), 'testing pop()')
        self.assertEqual(0, len(stack.get_values()), 'testing pop()')

    def test_push(self):
        stack = Stack()
        test = ['Hello', 'My', 'Stack']
        for t in test:
            stack.push(t)
            self.assertEqual(stack.get_values()[-1], t)

        self.assertEqual(3, len(stack.get_values()))

    def test_peek(self):
        stack = Stack()
        test = ['Hello', 'My', 'Stack']
        for t in test:
            stack.push(t)
            self.assertEqual(stack.peek(), t)
            stack.get_values().pop()

        self.assertEqual(0, len(stack.get_values()))

    def test_iterate(self):
        stack = Stack()
        test = ['Hello', 'My', 'Stack']
        l = len(test) -1

        """ Test generator """
        for t in test:
            stack.get_values().append(t)

        for i, e in enumerate(stack.iterate()):
            self.assertEqual(test[l -i], e)

        """ Test iterator """
        for t in test:
            stack.get_values().append(t)

        for i, e in enumerate(stack):
            self.assertEqual(test[l -i], e)

    def test_move_to(self):
        test = ['My', 'Stack']
        result = ['This is', 'My', 'Stack', '!']
        stack = Stack()

        for e in test:
            stack.push(e)

        stack1 = Stack()
        stack1.push('!')
        stack.move_to(stack1)
        stack1.push('This is')

        for i, e in enumerate(stack1):
            self.assertEqual(e, result[i])
