from src.nfa import NFA


def main():
    print('hello')
    regex = input("Enter regular expression: ")
    string = input("Enter string: ")
    nfa = NFA()
    nfa.build(regex)
    res = nfa.check(string)

    if res:
        print('yes')
    else:
        print('false')


main()
