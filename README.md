# Thompson_construction

## This is Graph theory project. Implementation of Thompson's construction algorithm.

At the moment ~~(I did'n check any algorithms yet)~~ my approach is to build graph representing regular expression using Thompson construction [rules](https://en.wikipedia.org/wiki/Thompson%27s_construction#Rules) and use it to validate words against given regular expression. By investigating [existing algorithms](https://en.wikipedia.org/wiki/Regular_expression#Implementations_and_running_times) I came to conclusion what my chosen approach is a good way to go. 


### An approach I wanted to take:
- convert regex to graph using Thompson Construction. Same way as if I would do it with pen and paper.
- travers constructed graph using matching string. If graph sink node is reached and here is no characters in a given string left return `True`. If here is no were to go in graph return `False`. I hope I can do it without converting a graph `NFA` to `DFA`.

### TODO list:
- &#x2611; Create `infix` to `postfix` converter. Make a custom one, because a provided one uses `.` as an `AND`. But I want to allow regex use `.` in regular expression to represent `ANY`.
- &#x2611; Create `Node` class to represent single vertices. Class fields `__parents` and `__childeren` represent incoming and outgoing edges. Class field `__guard` represents value which should be matched to enter that node.
- &#x2611; Create `Graph` class to represent graphs (collection of the vertices and edges - `Node`s)
- &#x2611; Implement all needed functionality for classes above to be useful.
- &#x2611; Use "Test Supported Development" approach (see bellow what I mean).
- &#x2611; Code should be readable and maintainable.

### Actual Implementation:
> I created a `Node` which can be empty or hold a value. Value should mach in order to pass is. `Node` have list of childrean and list of parients. Parients wasn't needed but it helped to debug and write the code. `Graph` has in and out `Node`s. In case of `Graph` of single `Node`, bouth in and out nodes are pointing to same `Node`. One `Graph` can contain another `Graph`. And is how `Graph` representing regular expresion is build. See below for `Graph` building blocks. `NFA` is used for buldding regex graph and for traversing it with given string.
<br>
 Here are test to confirm code correctness. [Main one](https://gitlab.com/My-/thompson_construction/blob/master/test/test_NFA.py) is for testing `NFA` class. CI status can be found [here](https://gitlab.com/My-/thompson_construction/pipelines).

### Graph building blocks:
<br>
<img src="img/none_or_more.jpg" width="30%"/>
<br>
[None, one or more](https://gitlab.com/My-/thompson_construction/blob/master/src/graph.py#L99).
<br><br>
<br>
<img src="img/one_or_more.jpg" width="30%"/>
<br>
[One or more](https://gitlab.com/My-/thompson_construction/blob/master/src/graph.py#L122).
<br><br>
<br>
<img src="img/none_or_one.jpg" width="30%"/>
<br>
[Only one or none](https://gitlab.com/My-/thompson_construction/blob/master/src/graph.py#L145).
<br><br>
<br>
<img src="img/and.jpg" width="30%"/>
<br>
[And](https://gitlab.com/My-/thompson_construction/blob/master/src/graph.py#L57).
<br><br>
<br>
<img src="img/or.jpg" width="30%"/>
<br>
[Or](https://gitlab.com/My-/thompson_construction/blob/master/src/graph.py#L76).
<br>

### Extras:
- Created custom `Stack` class. It was one of the first things I did. I did it mainly because I wanted to get familiar to with Python (it is a new language for me).
- Used some OOP. `Graph` class compose `Node` class. And `NFA` class extends `Graph` class.
- Code is well commented and in most places I used Python dock type comments.
- Made "silant" `AND` - *ab* instead of *a.b* (bouth reads "a AND b). Becaus of this I created `andify` function which was chalange on it's own. 
- Allowed `ANY` symbol.
- Added extra symbold like `?`, `+`.
- Fixed "star star" bug #3.
- Wrote test for most of code.
- Created my own implementation. Hardest and most time consuming part of the project.
- Used Git features. Creted extra branch because I wasnt sure what solution I worked would be worth to put in to master branch, but it was. 
- Loads of reasearch. Many of it done on paper (check [`img`](https://gitlab.com/My-/thompson_construction/tree/master/img) folder)
- Added CI. Now on each commit test are run automaticly. Noone teached me CI, so it took a [few trys](https://gitlab.com/My-/thompson_construction/commits/master) to get it working.

<br>
<br>


"Test Supported Development" approach - is an approach I came up with (I bet I'm not the only one). Is similar to [TDD](https://en.wikipedia.org/wiki/Test-driven_development) were:
>"... Test-driven development is related to the test-first programming concepts ..."

Difference is what inited of writing unit tests first, developer first implements some functionality and then he/she/it writes test for it. It feels more unnatural to use (test) something what does exist instead of of using (testing) something what is not build yet. It's why I call this approach Test **Supported** Development instead of Test **Driven** Development. Because development are supported by tests instead of being "driven" by them.


### Resources

##### Thompson's Construction:
- [Wikipedia](https://en.wikipedia.org/wiki/Thompson's_construction)
- [YouTube](https://www.youtube.com/watch?v=RYNN-tb9WxI)
- [Thompson's Algorithm rules](http://www.cs.may.ie/staff/jpower/Courses/Previous/parsing/node5.html)
- [StackOverflow](https://stackoverflow.com/questions/11819185/steps-to-creating-an-nfa-from-a-regular-expression)
- [Online infix to postfix onverter](https://www.mathblog.dk/tools/infix-postfix-converter/)
- [How to](https://swtch.com/~rsc/regexp/regexp1.html)
- []()

##### Regular expression:
- [Precedence](http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_04_08)
- [Online checker](https://regex101.com/)
- []()

### Python:
- [Classes](https://docs.python.org/3.7/tutorial/classes.html)
- [Data structures](https://docs.python.org/3/tutorial/datastructures.html#using-lists-as-stacks)
- [Pass value](https://stackoverflow.com/a/986145/5322506)
- [List of charsacters to string](https://stackoverflow.com/questions/4481724/convert-a-list-of-characters-into-a-string)
- [How to: switch](https://stackoverflow.com/questions/60208/replacements-for-switch-statement-in-python)
- [Iterator/generator](https://stackoverflow.com/a/24377/5322506)
- [PyCharm testing](https://confluence.jetbrains.com/display/PYH/Creating+and+running+a+Python+unit+test)
- [private](https://stackoverflow.com/questions/17193457/private-methods-in-python)
- [to string](https://stackoverflow.com/a/48222595/5322506)
- [Functional](https://docs.python.org/3.7/howto/functional.html)
- []()
